var texto = "Hello World";
//texto 1
    document.write(`<p> ${texto} <p>`);

//texto 2
document.write(`<p> ${texto.toUpperCase()} <p>`);

//texto 3
document.write(`<p> ${texto.toLowerCase()} <p>`);

//texto 4
document.write(`<p> ${texto.split("").reverse("").join("")} <p>`);

//texto 5
document.write(`<p class ="texto5"> ${texto} <p>`);

//texto 6
document.write(`<p> ${texto.fontcolor("yellow")} <p>`);

//texto 7
document.write(`<p> ${texto.fontsize(60)} <p>`);

//texto 8
document.write(`<p class ="texto8"> ${texto} <p>`);

//texto 9
document.write(`<ul class ="texto9"> <li> H </li> <li> E </li> <li> 
L </li> <li> L </li> <li> O </li> <li> W </li> <li> O </li> <li> R </li> <li> L </li> <li> D </li> <ul>`);

//texto 10
document.write(`<p class = "texto10"> ${texto.split("").reverse("").join("")} <p>`);
